/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.80063795853269, "KoPercent": 0.19936204146730463};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.1246012759170654, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.625560538116592, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [0.0036231884057971015, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.0, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [0.011811023622047244, 500, 1500, "suggestProgram"], "isController": false}, {"data": [0.24583333333333332, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.04065040650406504, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.032407407407407406, 500, 1500, "findAllClass"], "isController": false}, {"data": [0.3448275862068966, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [0.1323529411764706, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.2916666666666667, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [0.25, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.0, 500, 1500, "registrationByID"], "isController": false}, {"data": [0.0, 500, 1500, "leadByID"], "isController": false}, {"data": [0.016129032258064516, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.0, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.0, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [0.04878048780487805, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [0.0, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2508, 5, 0.19936204146730463, 116827.43580542257, 3, 450162, 84118.0, 293080.0999999999, 407946.85, 442448.58, 5.4980105882739805, 29.616799756529982, 11.293606582870233], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 223, 0, 0.0, 4260.179372197311, 3, 184658, 141.0, 11896.199999999999, 24050.8, 43135.95999999999, 0.7307258762156918, 0.4303004915606077, 0.9015353537794585], "isController": false}, {"data": ["getCentreHolidaysOfYear", 138, 0, 0.0, 133736.26086956516, 1088, 309603, 126539.0, 220806.80000000002, 245709.0499999999, 303117.29999999976, 0.3328581662409459, 0.8762424330545043, 0.42744969590512094], "isController": false}, {"data": ["getPastChildrenData", 166, 1, 0.6024096385542169, 315619.3795180723, 135873, 424621, 364141.0, 416827.8, 419098.6, 423159.73000000004, 0.3688938074729885, 0.7385037703280488, 0.8411787504388947], "isController": false}, {"data": ["suggestProgram", 127, 1, 0.7874015748031497, 71713.64566929136, 604, 248424, 58725.0, 156425.00000000003, 189384.39999999994, 244629.43999999997, 0.31548092209856915, 0.25493094197138316, 0.3915783710813295], "isController": false}, {"data": ["findAllWithdrawalDraft", 120, 0, 0.0, 14423.158333333331, 31, 186691, 5132.5, 35283.5, 54581.449999999924, 175034.31999999954, 0.38137976843891724, 0.2469285024169943, 1.2450708651282232], "isController": false}, {"data": ["findAllLevels", 123, 0, 0.0, 53013.11382113822, 237, 322655, 37734.0, 113398.20000000001, 135397.19999999998, 305487.32000000036, 0.318212628125857, 0.2026119468145105, 0.2666273778632669], "isController": false}, {"data": ["findAllClass", 108, 1, 0.9259259259259259, 72109.86111111111, 500, 308232, 66826.0, 129021.10000000003, 194747.34999999998, 300644.2799999997, 0.2679920694198716, 26.452919663483847, 0.21469026134189584], "isController": false}, {"data": ["getStaffCheckInOutRecords", 116, 0, 0.0, 9226.887931034478, 10, 94869, 2518.5, 31191.399999999994, 41642.39999999999, 91734.02999999997, 0.3835788568688722, 0.22737535753848187, 0.5019488947307508], "isController": false}, {"data": ["getChildrenToAssignToClass", 102, 0, 0.0, 27903.6568627451, 93, 162571, 18538.0, 66135.40000000001, 86644.45, 161506.20999999996, 0.24414296245942918, 0.1389993624158664, 0.1790540671943665], "isController": false}, {"data": ["getCountStaffCheckInOut", 120, 0, 0.0, 15281.925000000001, 14, 198648, 3945.0, 36882.00000000001, 49905.74999999995, 192285.20999999976, 0.2913639718736646, 0.17214375291363973, 0.2970546744493221], "isController": false}, {"data": ["searchBroadcastingScope", 123, 1, 0.8130081300813008, 208251.12195121945, 76989, 431729, 204844.0, 308542.60000000003, 341658.8, 431189.72000000003, 0.2696410485748484, 0.5178438868757153, 0.528485922353243], "isController": false}, {"data": ["getTransferDrafts", 146, 0, 0.0, 12114.527397260268, 20, 97202, 3900.5, 35452.4, 49799.950000000004, 91339.22000000002, 0.4572573951987973, 0.29382359964922716, 0.7935023352229129], "isController": false}, {"data": ["registrationByID", 132, 0, 0.0, 197804.24242424246, 3541, 413068, 192974.5, 313487.60000000003, 352849.8, 408290.5899999998, 0.30592734225621415, 0.4103361832956718, 1.3916108986615678], "isController": false}, {"data": ["leadByID", 108, 0, 0.0, 121160.05555555553, 4247, 329730, 122287.0, 204441.2, 264040.55, 328903.88999999996, 0.2549725550374786, 0.3577021114914714, 1.1513604438411142], "isController": false}, {"data": ["getRegEnrolmentForm", 124, 0, 0.0, 110385.54838709674, 708, 377701, 105548.0, 182075.0, 248340.75, 354655.5, 0.29917220979692477, 0.5435596396181211, 1.2790780610263055], "isController": false}, {"data": ["findAllLeads", 147, 1, 0.6802721088435374, 221720.46258503402, 101028, 400435, 212642.0, 320076.2, 352175.0, 395438.2000000001, 0.33025692640887155, 0.5370251257840793, 0.7475933158357073], "isController": false}, {"data": ["getEnrollmentPlansByYear", 129, 0, 0.0, 347294.5116279068, 145471, 450162, 390745.0, 446124.0, 447665.5, 449955.89999999997, 0.2862570010917709, 0.31941484367372025, 0.4749518016161316], "isController": false}, {"data": ["getAvailableVacancy", 123, 0, 0.0, 65161.365853658535, 81, 220320, 51305.0, 126906.40000000001, 167840.59999999998, 219573.84000000003, 0.313873996177372, 0.17195635923389227, 0.41533131330111234], "isController": false}, {"data": ["getRegistrations", 133, 0, 0.0, 185202.58646616538, 24525, 377986, 180255.0, 284971.2, 332511.1, 375632.18, 0.3084236767348832, 0.538357748999942, 0.9523785799176764], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 4, 80.0, 0.1594896331738437], "isController": false}, {"data": ["Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, 20.0, 0.03987240829346093], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2508, 5, "502/Bad Gateway", 4, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["getPastChildrenData", 166, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["suggestProgram", 127, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllClass", 108, 1, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["searchBroadcastingScope", 123, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllLeads", 147, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
